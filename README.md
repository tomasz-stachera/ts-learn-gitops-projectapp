# GitOps Example App

This repository contains Kubernetes and Helm manifest files for [Project App](https://github.com/dnaprawa/projectapp) using GitOps approach.
It's used as part of [Kubernetes Maestro](https://kubernetesmaestro.pl)

My ArgoCD successfull deploy to GKE:

![argo-success](argo-success.png "argo-success")
